#include "String.h"

//FUNCIONES STRING


void string_init(String *string) {
    string->size = 0;
    string->capacity = STRING_CAPACITY;
    string->string = malloc(sizeof (char) * string->capacity);
}

void string_append(String *string, char value) {
    string_double_capacity_if_full(string);
    string->string[string->size++] = value;
}

void string_double_capacity_if_full(String *string) {
    if (string->size >= string->capacity) {
        string->capacity *= 2;
        string->string = realloc(string->string, sizeof (char) * string->capacity);
    }
}

void string_free(String *string) {
    free(string->string);
}
