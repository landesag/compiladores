CFILES := main.c AnalizadorLexico.c sistemaEntrada.c tabla_hash_encadenamiento.c lista.c String.c Errores.c
PROG := lexico
CFLAGS := -g
LDFLAGS :=

CFLAGS += -MMD
CC := gcc

OBJFILES := ${CFILES:.c=.o}
DEPFILES := ${CFILES:.c=.d}

$(PROG) : $(OBJFILES)
		$(LINK.o) $(LDFLAGS) -o $@ $^
clean:
		rm -f $(PROG) $(OBJFILES) $(DEPFILES)
-include $(DEPFILES)
