#include "AnalizadorLexico.h"

int isDelimiter(char caracter) {

    int is = 0;
    switch (caracter) {
        case '{':

        case '}':
        case '(':
        case ')':
        case ':':
        case ',':
        case '[':
        case ']':
            is = 1;
            break;
    }
    return is;
}

int isOperator(char caracter) {

    int is = 0;
    switch (caracter) {
        case '=':
        case '+':
        case '-':
        case '*':
            is = 1;
            break;
    }
    return is;
}

int lastWasScape = 0; //Variable global usada para saber si el ultimo caracter fue un \n o un espacio despues de un \n, lo que implica que hay que devolver los espacios al sintáctico

/*
*
*SIGUIENTECOMPONENTELEXICO() es la funcion principal, que permite obtener un lexema en cada llamada, pasando por una serie de estados
*como un gran automata, estando cada conjunto de estados dedicado a reconocer algo en concreto
*
*/


EstructuraLexica siguienteComponenteLexico() {
    int erro = 0; //Usado por si algun error debiera detener la ejecución antes de devolver un lexema (no usado)
    //CONTADORES UTILIZADOS POR AHORRO DE ESTADOS
    int contadorTresComillas = 0; //Contador usado para evitar el uso de 3 estados para determinar cuando termina un string documental
    char lastChar; //Usado para guardar el caracter anterior sólo en el caso de que sólo pueda haber dos caracteres en el lexema como máximo
    int firstLap = 1;
    String lexema; //Se utiliza un string con memoria reservada dinamica, pero el tamaño inicial es 128 por lo que nunca debería superarse
    string_init(&lexema);
    EstructuraLexica estructuraLexica;
    char actualChar;

    int estado = 0;
	//INICIO DEL AUTOMATA
    while (!erro) { 

        actualChar = siguienteCaracter(); //EN CUALQUIER CASO SE PIDE EL SIGUIENTE CARACTER DEL ARCHIVO EN EL SISTEMA DE ENTRADA

        switch (estado) {

                /*
                 * 
                 * 
                 * 0 - ESTADO INICIAL - SE EMPIEZA A LEER UN NUEVO LEXEMA
                 * 
                 * 
                 */

            case 0: //SEGÚN LO QUE LLEGUE SE IRÁ AL ESTADO EN EL QUE COMIENZA EL "AUTÓMATA" CORRESPONDIENTE

                if (actualChar == ' ' && lastWasScape == 1) { //VAMOS A CONTAR UN ESTADO PARA CONTAR ESPACIOS DE IDENTACION SI VENIMOS DE UN \N
                    string_append(&lexema, actualChar);
		    estado = 11;
                    
                } else {
                    lastWasScape = 0; //SE VOLVERA A PONER A 1 SI LUEGO LLEGA UN \n
                    if (actualChar == '#') { //EMPIEZA UN COMENTARIO
                        estado = 10;
                    } else if (isalpha(actualChar) || actualChar == '_') { //LA CADENA COMIENZA POR UN CARACTER ALFABETICO
                        estado = 1;
                        string_append(&lexema, actualChar);
                    } else if (actualChar == '\"') { //EMPIEZAN UNAS COMILLAS DOBLES
                        estado = 15;
                        string_append(&lexema, actualChar);
                    } else if (isDelimiter(actualChar)) { //ES UN DELIMITADOR
                        string_append(&lexema, actualChar);
                        estructuraLexica.lexema = lexema.string;
                        estructuraLexica.compLexico = (int) lexema.string[0]; //ASCII PARA IDENTIFICAR
                        return estructuraLexica; //DELIMITADOR
                    } else if (actualChar == '\n') {
                        lastWasScape = 1;
                        string_append(&lexema, actualChar);
                        estructuraLexica.lexema = lexema.string;
                        estructuraLexica.compLexico = (int) lexema.string[0]; //ASCII PARA IDENTIFICAR
                        return estructuraLexica;
                    } else if (isdigit(actualChar) && actualChar != '0') { //ES UN NÚMERO Y NO VA A SER HEX
                        string_append(&lexema, actualChar);
                        estado = 20;
                    } else if (actualChar == '0') {
                        string_append(&lexema, actualChar);
                        estado = 26;
                    } else if (actualChar == '.') { //PUEDE SER DELIMITADOR . O EL INICIO DE UN FLOAT
                        string_append(&lexema, actualChar);
                        estado = 25;
                    } else if (actualChar == '\'') { //EMPIEZA UNA CADENA DE COMILLAS SIMPLES
                        string_append(&lexema, actualChar);
                        estado = 19;
                    } else if (actualChar == '/' || actualChar == '*' || actualChar == '>' || actualChar == '<' || actualChar == '+' || actualChar == '-' || actualChar == '=') { //EMPIEZA UN OPERADOR O COMPARADOR
                        lastChar = actualChar; //Usamos lastChar para ahorrar muchos estados
                        string_append(&lexema, actualChar);
                        estado = 5;
                    } else if (actualChar == EOF) {
                        string_append(&lexema, actualChar);
                        estructuraLexica.lexema = lexema.string;
                        estructuraLexica.compLexico = END_OF_FILE;
                        //introducir en tabla
                        return estructuraLexica; //FINAL DE FICHERO
                    } else if (actualChar == ' '  || actualChar == '\t'){ //No se realiza acción pero tampoco son caracteres inválidos

		    } else {
                        mostrarError(ERROR_NO_RECONOCIDO);
                    }
                }
                break;



                /*
                 *
                 *  ESTADO 1 PARA IDENTIFICADORES
                 * 
                 */

            case 1:
                if (isalnum(actualChar) || actualChar == '_') {
                    string_append(&lexema, actualChar);
                } else {
                    devolverCaracter();
                    estructuraLexica.lexema = lexema.string;
                    //Intentamos insertar en la tabla hash como un identificador generico
                    estructuraLexica.compLexico = ID;
                    int possibleId = InsertarHash(&tab, estructuraLexica);
                    if (possibleId != -1) { //Si no se insertó (ya estaba, podia ser una palabra reservada) se recoge su id
                        estructuraLexica.compLexico = possibleId;
                    }
                    return estructuraLexica;
                }
                break;



                /*
                 *
                 *  ESTADO 5 DEDICADO A OPERADORES
                 *
                 */

            case 5: //LLEGÓ UN +, -, >, <, *, / o =
                if (actualChar == '=' && lastChar == '*') { //Si llega un igual se añade y se devuelve en todos los casos
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MULT_IGUAL;
                    return estructuraLexica;
                } else if (actualChar == '=' && lastChar == '*') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MAY_IGUAL;
                    return estructuraLexica;
                } else if (actualChar == '=' && lastChar == '*') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MEN_IGUAL;
                    return estructuraLexica;
                } else if (actualChar == '=' && lastChar == '*') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MAS_IGUAL;
                    return estructuraLexica;
                } else if (actualChar == '=' && lastChar == '*') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MENOS_IGUAL;
                    return estructuraLexica;
                } else if (actualChar == '=' && lastChar == '=') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = IGUAL_IGUAL;
                    return estructuraLexica;
                } else if (actualChar == '*' && lastChar == '*') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = EXP;
                    return estructuraLexica;
                } else if (actualChar == '+' && lastChar == '+') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MAS_MAS;
                    return estructuraLexica;
                } else if (actualChar == '-' && lastChar == '-') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = MENOS_MENOS;
                    return estructuraLexica;
                } else { //Si llega cualquier otra cosa se devuleve el +, -, >, <, * o = sólo
                    devolverCaracter();
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = (int) lexema.string[0]; //ASCII PARA IDENTIFICAR
                    return estructuraLexica; //OPERADOR
                }

                break;


                /*
                 *
                 *  ESTADO 10 PARA LOS COMENTARIOS Y 11 PARA IDENTACIONES
                 * 
                 */


            case 10: //Estamos en un comentario
                if (actualChar == '\n') {
                    estado = 0;
                    devolverCaracter();
                }
                break;
	    case 11: //Espacios a contar como identacion
		if(actualChar == ' ') {
                    string_append(&lexema, actualChar);
		}else{
                    devolverCaracter();
		    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = IDENTACION;
                    return estructuraLexica;
		}
		break;

                /*
                 * 
                 * ESTADOS 20 A 27 PARA LOS NUMEROS
                 * 
                 * 
                 */


                //ESTADO INICIAL DE RECONOCIMIENTO DE NUMEROS

            case 26: //NOS LLEGO UN 0
                if (isdigit(actualChar)) { //Si llega otro lo anadimos y vamos al estado de numeros normales
                    string_append(&lexema, actualChar);
                    estado = 20;
                } else if (actualChar == 'x') { //SI LLEGA UN x VA A SER UN HEXADECIMAL
                    string_append(&lexema, actualChar);
                    estado = 27;
                } else { //SI NO, DEVOLVEMOS EL 0 SOLO, ES UN ENTERO
                    devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = INTEGER; //ENTERO
                    return estructuraLexica;
                }
                break;

            case 27: //ES UN HEXADECIMAL, LLEVAMOS 0x (aunque puede aun haber error si es la primera vuelta y llega un valor no válido)
                if (isdigit(actualChar) || actualChar == 'a' || actualChar == 'b' || actualChar == 'c' || actualChar == 'd' || actualChar == 'e' || actualChar == 'f' || actualChar == 'A' || actualChar == 'B' || actualChar == 'C' || actualChar == 'D' || actualChar == 'E' || actualChar == 'F') {
                    string_append(&lexema, actualChar);
                    firstLap = 0;
                } else {
                    if (!firstLap) {
                        devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                        estructuraLexica.lexema = lexema.string;
                        estructuraLexica.compLexico = HEX;
                        return estructuraLexica;
                    } else {
                        devolverCaracter();
                        devolverCaracter();
                        lexema.string[lexema.size - 1] = '\0';
                        estructuraLexica.lexema = lexema.string;
                        estructuraLexica.compLexico = INTEGER;
                        return estructuraLexica;
                    }
                }
                break;
            case 20: //YA NOS LLEGÓ UN DÍGITO (O DOS SI EL PRIMERO FUE UN 0), SABEMOS QUE VA A SER UN NUMERO
                if (isdigit(actualChar)) { //Si llega otro lo anadimos y seguimos en este estado
                    string_append(&lexema, actualChar);

                } else if (actualChar == '.') { //Si llega un punto es que empieza un float
                    string_append(&lexema, actualChar);
                    estado = 21;
                } else if (actualChar == 'e') { //Nos llego una e, numero cientifico
                    string_append(&lexema, actualChar);
                    estado = 22;
                } else { //En cualquier otro caso devolvemos un entero
                    devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = INTEGER; //ENTERO
                    return estructuraLexica;
                }
                break;

                //ESTADO SOBRE NUMEROS DECIMALES

            case 21: //LLEGARON DIGITOS Y UN PUNTO, ES UN FLOAT
                if (isdigit(actualChar)) { //Si llega otro digito lo anadimos y seguimos en este estado
                    string_append(&lexema, actualChar);
                } else {//En cualquier otro caso devolvemos un float
                    devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = FLOAT; //FLOAT
                    return estructuraLexica;
                }
                break;

                //ESTADOS SOBRE NUMEROS CIENTIFICOS

            case 22: //LLEGARON DIGITOS Y UNA E, SI LLEGA OTRO DIGITO O UN MENOS ES UN CIENTIFICO Y SI HAY QUE VOLVER DOS POSICIONES ATRAS
                if (isdigit(actualChar)) { //Si llega otro digito lo anadimos y vamos a un estado donde ya se permite que llegen otros caracteres
                    string_append(&lexema, actualChar);
                    estado = 24;
                } else if (actualChar == '-' || actualChar == '+') { //Va a un estado distinto pues aun puede haber error si luego no viene un digito
                    string_append(&lexema, actualChar);
                    estado = 23;
                } else {//En cualquier otro caso hay que volver atras dos posiciones
                    devolverCaracter();
                    devolverCaracter();
                    lexema.string[lexema.size - 1] = '\0';
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = INTEGER;
                    return estructuraLexica;
                }
                break;
            case 23: //LLEGARON DIGITOS, UNA E Y UN -, ES DECIR, TIENE QUE LLEGAR UN DIGITO O retrocedemos
                if (isdigit(actualChar)) { //Si llega otro digito lo anadimos y vamos a un estado donde ya se permite que llegen otros caracteres
                    string_append(&lexema, actualChar);
                    estado = 24;
                } else {//En cualquier otro caso atras 3 posiciones y devolvemos el numero antes de la e
                    devolverCaracter();
                    devolverCaracter();
                    devolverCaracter();
                    lexema.string[lexema.size - 2] = '\0';
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = INTEGER;
                    return estructuraLexica;
                }
                break;
            case 24://ESTADO FINAL DE NUMERO CIENTIFICO: ACEPTA DIGITOS Y TERMINA CON OTRO CARACTER
                if (isdigit(actualChar)) { //Si llega otro digito lo anadimos y seguimos en este estado
                    string_append(&lexema, actualChar);
                } else {//En cualquier otro caso devolvemos un numero cientifico
                    devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = CIENTIFICO; //CIENTIFICO
                    return estructuraLexica;
                }
                break;

                // ESTADO EN EL QUE UN PUNTO RECIBIDO EN EL ESTADO 0 PUEDE SER DELIMITADOR O FLOAT

            case 25: //DE MOMENTO LLEGÓ UN PUNTO, SI DESPUES VIENE UN DIGITO ES UN FLOAT, SI NO ES UN DELIMITADOR
                if (isdigit(actualChar)) {
                    string_append(&lexema, actualChar);
                    estado = 21;
                } else {
                    devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = (int) lexema.string[0]; //ASCII DEL PUNTO
                    return estructuraLexica;
                }
                break;


                /*
                 * 
                 * FIN DE LOS ESTADOS SOBRE NÚMEROS
                 * 
                 * 
                 */


                /*
                 * 
                 * 
                 * ESTADOS 15 A 18 DEDICADOS A LOS STRINGS DE COMILLAS DOBLES
                 * 
                 * 
                 */


            case 15: //Hemos recibido una sola comilla doble
                if (actualChar == '\"') { //Si llega otra vamos a un estado donde podrá acabar el lexema o empezar un string documental
                    estado = 17;
                    string_append(&lexema, actualChar);
                } else { //Si llega cualquier otra cosa vamos a un estado donde añadir al lexema caracteres
                    estado = 16;
                    string_append(&lexema, actualChar);
                }
                break;

            case 16: //Recibimos ya comillas y un caracter que no es comillas
                if (actualChar == '\"') { //Si llega otra acaba el string
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = STRING;
                    return estructuraLexica;
                } else if (actualChar=='\n'){ //Si llega un \n error, la cadena esta cortada
		     mostrarError(ERROR_CUT_STRING);
	    	     estructuraLexica.lexema = NULL;
                     estructuraLexica.compLexico = 0;
                     return estructuraLexica;	 
		} else {
                    string_append(&lexema, actualChar);
                }
                break;

            case 17: //Recibimos comillas despues de las primeras, puede ser string cerrado o documental
                if (actualChar == '\"') { //Si llega otra es que es un documental
                    estado = 18;
                    string_append(&lexema, actualChar);
                } else {
                    devolverCaracter(); //Debemos devolver el caracter pues podría ser parte del siguiente lexema
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = STRING; //STRING
                    return estructuraLexica;
                }
                break;

            case 18: //Tres comillas seguidas, es un documental
                if (contadorTresComillas < 2 && actualChar == '\"') { //Si aun no me llegaron 2 y me llega una
                    string_append(&lexema, actualChar);
                    contadorTresComillas += 1;
                } else if (contadorTresComillas == 2 && actualChar == '\"') { //Si me llega la tercera
                    contadorTresComillas = 0;
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = STRING_DOCUMENTAL; //STRING DOCUMENTAL
                    return estructuraLexica;

                } else { //Si me llega cualquie otra cosa reseteo el contador
                    string_append(&lexema, actualChar);
                    contadorTresComillas = 0;
                }
                break;

                /*
                 *
                 *  ESTADO 19 DEDICADO A STRINGS COMILLAS SIMPLES
                 *
                 */
            case 19:
                if (actualChar == '\'') {
                    string_append(&lexema, actualChar);
                    estructuraLexica.lexema = lexema.string;
                    estructuraLexica.compLexico = STRING; //STRING COMILLAS SIMPLES
                    return estructuraLexica;
                } else if (actualChar == '\n'){
                     mostrarError(ERROR_CUT_STRING);
	    	     estructuraLexica.lexema = NULL;
                     estructuraLexica.compLexico = 0;
                     return estructuraLexica;	 
                } else {
                    string_append(&lexema, actualChar);
		}
                break;
        }
	

        if (lexema.size > 128) { //SI EL TAMAÑO ES MAYOR DEL PERMITIDO EN UN BUFFER, HAY UN ERROR Y SE DEVUELVE UN LEXEMA NULO PARA QUE EL ANALIZADOR SINTACTICO (MAIN) NO DETECTE NINGUN LEXEMA CORRECTO
            mostrarError(ERROR_SIZE);
	    estructuraLexica.lexema = NULL;
            estructuraLexica.compLexico = 0;
            return estructuraLexica;	    
        }
    }
}
