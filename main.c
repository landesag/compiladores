#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "AnalizadorLexico.h"
#include "Errores.h"
/*
 * 
 */
EstructuraLexica crearEstructuraLexica(char * lexema, int comp) {
    EstructuraLexica estructura;
    estructura.lexema = lexema;
    estructura.compLexico = comp;
    return estructura;
}

void InsertarReservadasEnHash(TablaHash * tab) {
    InsertarHash(tab, crearEstructuraLexica("import", IMPORT));
    InsertarHash(tab, crearEstructuraLexica("return", RETURN));
    InsertarHash(tab, crearEstructuraLexica("for", FOR));
    InsertarHash(tab, crearEstructuraLexica("def", DEF));
    InsertarHash(tab, crearEstructuraLexica("if", IF));
    InsertarHash(tab, crearEstructuraLexica("elif", ELIF));
    InsertarHash(tab, crearEstructuraLexica("else", ELSE));
    InsertarHash(tab, crearEstructuraLexica("not", NOT));
    InsertarHash(tab, crearEstructuraLexica("in", IN));
    InsertarHash(tab, crearEstructuraLexica("as", AS));
}

int main(int argc, char** argv) {


    InicializarTablaHash(tab);
    InsertarReservadasEnHash(&tab);
    int correcto = abrirArchivo();
    if(correcto == 0){
    	  mostrarError(ERROR_APERTURA);
    }
    EstructuraLexica next;

    int opc;
    printf("Mostrar los lexemas impresos como codigo o con sus identificadores?\n\n");
    printf("1)Como codigo\n");
    printf("2)Con identificadores\n");
    scanf("%d", &opc);

    if (opc != 1 && opc != 2) {
        printf("Impresion como codigo por defecto:\n\n");
    }

    next = siguienteComponenteLexico();
    while(next.lexema == NULL){
		mostrarError(ERROR_NULL_LEX);
    	next = siguienteComponenteLexico();
    }

    while (next.compLexico != END_OF_FILE) {
        if (opc == 2) {
			if(next.compLexico == (int)'\n'){
            printf("\n\\n ----- %d\n", next.lexema, next.compLexico);
			} else if (next.compLexico == IDENTACION){
            printf("\n%s ----- %d  IDENTACION\n", next.lexema, next.compLexico);
			} else {
            printf("\n%s ----- %d\n", next.lexema, next.compLexico);
			}
        } else {
            printf("%s ", next.lexema);
        }
		
        next = siguienteComponenteLexico();

		while(next.lexema == NULL){
			mostrarError(ERROR_NULL_LEX);
    		next = siguienteComponenteLexico();
    	}
    }


    int i;
    printf("\n\n\n---------TABLA DE SIMBOLOS---------\n\n");
    int j;
    EstructuraLexica imprimir;
    for (i = 0; i < Tam; i++) {
        if (existe(tab[i])) {
            if (!esvacia(tab[i])) {
                posicion pos = primero(tab[i]);
                while (pos != fin(tab[i])) {
                    recupera(tab[i], pos, &imprimir);
                    printf("%s     ------     %d\n", imprimir.lexema, imprimir.compLexico);
                    pos = siguiente(tab[i], pos);
                }
            }
        }
    }


    DestruirTablaHash(tab);
    
    return;
}

