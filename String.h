#include <stdio.h>
#include <stdlib.h>
#define STRING_CAPACITY 128


typedef struct {
    int size;
    int capacity;
    char *string;
} String;


//Funicones String
void string_init(String *string);

void string_append(String *string, char value);

void string_double_capacity_if_full(String *string);

