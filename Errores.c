#include "Errores.h"


void mostrarError(int code){
	switch (code){
		case ERROR_SIZE:
			printf("\nError: Lexema demasiado largo.\n");
		break;

		case ERROR_NO_RECONOCIDO:
			printf("\nError: Caracter no reconocido.\n");
		break;

		case ERROR_APERTURA:
			printf("\nError: No se pudo abrir el archivo.\n");
			exit(1);
		break;

		case ERROR_NULL_LEX:
			printf("\nError: Lexema nulo\n");
		break;

		case ERROR_CUT_STRING:
			printf("\nError: String sin finalizar\n");
		break;
	}
}
