#include "sistemaEntrada.h"

FILE* archivo;
char returnedChar = NULL;
char * DobleBuffer;
int DoubleBufferPosition;
int initialPos = 0; //Posicion inicial del siguiente buffer a cargar
int actualPos = 0;
int banderaCargarSiguiente = 1;

void cargarBuffer() { //Funcion usada para cargar uno de los dos buffers, el primero o el segundo segun del cual se venga
    int readSize;

    readSize = fread(DobleBuffer + initialPos, 1, 128, archivo);
    if (readSize < 128) {
        DobleBuffer[initialPos + readSize ] = '\0';
    }
    actualPos = initialPos;

    if (initialPos == 0) {
        initialPos = 129;
    } else {
        initialPos = 0;
    }

}

int abrirArchivo() { //Funcion utilizada para la abertura del archivo wilcoxon.py, que sera el analizado por el programa, asi como la inicializacion del doble buffer
    archivo = fopen("wilcoxon.py", "r");
    if(archivo==NULL){
	return 0;
    }
    DobleBuffer = (char *) malloc(sizeof (char)*258);
    DobleBuffer[128] = EOF;
    DobleBuffer[257] = EOF;
    cargarBuffer();
    return 1;
}

void devolverCaracter() { //Funcion usada para devolver un caracter al flujo desde el analizador lexico (volver al caracter anterior)

    if (actualPos > 0 && actualPos != 128) {
        banderaCargarSiguiente = 1;
        actualPos--;
    } else if (actualPos == 129) { //Si la posicion es la inicial de un buffer y se manda volver atras, se activa una bandera conforme no se permite cargar de nuevo el buffer siguiente (que ya ha sido cargado)
        banderaCargarSiguiente = 0;
        actualPos = 127;
    } else if (actualPos == 0) {
        banderaCargarSiguiente = 0;
        actualPos = 256;
    }

}

char siguienteCaracter() {

    char actual = DobleBuffer[actualPos];
    if (actual == EOF && banderaCargarSiguiente) {
        cargarBuffer();
    } else if (actual == EOF && banderaCargarSiguiente == 0){ //Si no se puede cargar el buffer se pasa igualmente a la posicion inicial del siguiente buffer
	if (actualPos == 128) {
            actualPos = 129;
        } else {
            actualPos = 0;
        }
    }
    if (actual == '\0') {
        actual = EOF;
        return actual;
    }
    actual = DobleBuffer[actualPos];
    actualPos++;
    return actual;
}
